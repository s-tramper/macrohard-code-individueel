using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using cinema.Controllers;
using cinema.Models;
using cinema.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace cinema_unit_testing;

public class ArrangementTests
{
    
    [Fact]
    public async Task EditTest()
    {
        // Arrange
        var arrangementRepo = new Mock<IArrangementRepository>();
        var fakeArrangement = new Arrangement();
        var controller = new ArrangementController(arrangementRepo.Object);
        
        arrangementRepo.Setup(s => s.FindById(1)).Returns(
            fakeArrangement
        );
        
        // Act
        var result =  controller.Edit(10,1);

        // Assert
        Assert.IsType<RedirectToActionResult>(result);
    }
    
    
    [Fact]
    public async Task ArrangementsTest()
    {
        // Arrange
        var arrangementRepo = new Mock<IArrangementRepository>();
        var fakeArrangement = new Arrangement();
        var controller = new ArrangementController(arrangementRepo.Object);
        
        var list = new List<Arrangement>();
        list.Add(fakeArrangement);
        var queryable = list.AsQueryable();
        
        arrangementRepo.Setup(s => s.FindAllArrangements()).Returns(
            queryable
        );
        
        // Act
        var result =  controller.Arrangements(fakeArrangement.Name,10);

        // Assert
        Assert.IsType<ViewResult>(result);
    }
    
    
}