using System.Threading.Tasks;
using cinema.Controllers;
using cinema.Models;
using cinema.Repositories;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace cinema_unit_testing;

public class VoucherControllerTests
{
    [Fact]
    public async Task IndexTest()
    {
        // Arrange
        var userRepo = new Mock<IUserRepositroy>();
        var voucherRepo = new Mock<IVoucherRepository>();
        
        var controller = new VoucherController(voucherRepo.Object, userRepo.Object);

        // Act
        var result =  controller.Index("10 rittenkaart", 10);

        // Assert
        Assert.IsType<ViewResult>(result);
    }
    
    
    [Fact]
    public async Task CreateTest()
    {
        // Arrange
        var userRepo = new Mock<IUserRepositroy>();
        var voucherRepo = new Mock<IVoucherRepository>();
        
        var controller = new VoucherController(voucherRepo.Object, userRepo.Object);

        // Act
        var result =  controller.Create(1);

        // Assert
        Assert.IsType<ViewResult>(result);
    }
    
    [Fact]
    public async Task OverviewTest()
    {
        // Arrange
        var userRepo = new Mock<IUserRepositroy>();
        var voucherRepo = new Mock<IVoucherRepository>();
        var fakeVoucherType = new VoucherType();
        
        var controller = new VoucherController(voucherRepo.Object, userRepo.Object);
        VoucherType voucherType = voucherRepo.Object.FindById(1);

        voucherRepo.Setup(s => s.FindById(1)).Returns(
            fakeVoucherType
         );

        // Act
        var result =  controller.Overview(1,10);

        // Assert
        Assert.IsType<ViewResult>(result);
    }
}