using System.ComponentModel.DataAnnotations;
using cinema.Identity;

namespace cinema.Models;

public class Voucher
{
    [Key]
    public int Id { get; set; }
    
    public CinemaIdentityUser user { get; set; }

    public VoucherType VoucherType { get; set; }

    public int code { get; set; }

}