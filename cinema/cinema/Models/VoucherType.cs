using System.ComponentModel.DataAnnotations;

namespace cinema.Models;

public class VoucherType
{
    [Key]
    public int Id { get; set; }

    public string name { get; set; }
    
    public string description { get; set; }
    
    public double price { get; set; }
    
    public DateTime purchasedAt { get; set; }
}