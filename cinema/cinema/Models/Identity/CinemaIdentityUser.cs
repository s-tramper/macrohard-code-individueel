using Microsoft.AspNetCore.Identity;

namespace cinema.Identity;

public class CinemaIdentityUser : IdentityUser
{
    public CinemaIdentityUser()
    {
    }

    public CinemaIdentityUser(string userName) : base(userName)
    {
    }
}