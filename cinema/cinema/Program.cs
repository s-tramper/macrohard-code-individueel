using cinema.Data;
using cinema.Models;
using cinema.Services;
using Microsoft.EntityFrameworkCore;
using cinema.Models;
using cinema.Repositories;
using Microsoft.AspNetCore.Identity;
using cinema.Areas.Identity.Data;
using cinema.Identity;
using Microsoft.Extensions.DependencyInjection.Extensions;

var builder = WebApplication.CreateBuilder(args);

IronBarCode.License.LicenseKey = "IRONBARCODE.SEBASTIANTRAMPER.29166-5720797EB6-KEQMBA73BTVX7UDH-4XOIJUBZ3IPT-HGMNNB5KPMVA-4HQ7XCXW6G7A-ZFCWZG4HQI5Y-MRNJZT-TPNGFO7XQG2FUA-DEPLOYMENT.TRIAL-CYEAL5.TRIAL.EXPIRES.20.MAY.2022";
// Add services to the container.
builder.Services.AddRazorPages();
builder.Services.AddControllers();

builder.Services.AddScoped<ITicketService, TicketService>();
builder.Services.AddScoped<ISeatService, SeatService>();
builder.Services.AddScoped<IPriceCalculatingService, PriceCalculatingService>();
builder.Services.AddScoped<IShowService, ShowService>();
builder.Services.AddScoped<ITimeService, TimeService>();
builder.Services.AddScoped<IRoomService, RoomService>();

builder.Services.AddScoped<IPaymentAdapter, PaymentAdapter>();
builder.Services.AddScoped<IMovieService, MovieService>();

builder.Services.AddScoped<IShowRepository, ShowRepository>();
builder.Services.AddScoped<ITicketRepository, TicketRepository>();
builder.Services.AddScoped<IRoomRepository, RoomRepository>();
builder.Services.AddScoped<IMovieRepository, MovieRepository>();
builder.Services.AddScoped<IHomeRepository, HomeRepository>();
builder.Services.AddScoped<IPaymentRepository, PaymentRepository>();
builder.Services.AddScoped<IRoomRepository, RoomRepository>();
builder.Services.AddScoped<IRoomTemplatesRepository, RoomTemplatesRepository>();
builder.Services.AddScoped<ISubscriberRepository, SubscriberRepository>();
builder.Services.AddScoped<IArrangementRepository, ArrangementRepository>();
builder.Services.AddScoped<IVoucherRepository, VoucherRepository>();
builder.Services.AddScoped<IUserRepositroy, UserRepository>();


// Add DbContext
var connectionString = builder.Configuration.GetConnectionString("CinemaDbContext");

// builder.Services.AddDbContext<cinemaIdentityDbContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddDbContext<CinemaContext>(options => options.UseSqlServer(connectionString));

builder.Services
    .AddIdentity<CinemaIdentityUser, CinemaIdentityRole>()
    .AddEntityFrameworkStores<CinemaContext>();


builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Beheerder",
        policy => policy.RequireRole("Beheerder"));
    options.AddPolicy("Back office Manager",
        policy => policy.RequireRole("Back office Manager"));
    options.AddPolicy("Kassamedewerker",
        policy => policy.RequireRole("Kassamedewerker"));
    options.AddPolicy("Manager",
        policy => policy.RequireRole("Manager"));
    options.AddPolicy("Abonnementhouder",
        policy => policy.RequireRole("Abonnementhouder"));
});

builder.Services.Configure<IdentityOptions>(
    options =>
    {
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireDigit = false;
        options.Password.RequiredUniqueChars = 5;
        options.Password.RequiredLength = 8;
    });

builder.Services.ConfigureApplicationCookie(
    options =>
    {
        options.LoginPath = "/Identity/Account/Login";

        // todo lesson 4-18 configureren: acces denied
        options.AccessDeniedPath = "/Users/AccessDenied";
    });
    
var app = builder.Build();

using (var scope = app.Services.CreateScope())
{
    var services = scope.ServiceProvider;
    SeedData.Initialize(services);
    
    SeedIdentityData.Initialize(app.Services,app.Configuration);
}

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcorehsts.
    // app.UseHsts();
}

app.UseStaticFiles();

app.UseRouting();
app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();

app.MapRazorPages();
app.UseHttpsRedirection();

app.MapControllerRoute(
    name: "Movies",
    pattern: "{controller=Movies}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "RoomTemplates",
    pattern: "{controller=RoomTemplates}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "Rooms",
    pattern: "{controller=Rooms}/{action=Index}/{id?}");

app.MapControllerRoute(
    name: "Shows",
    pattern: "{controller=Shows}/{action=Index}/{id?}");

app.Run();
