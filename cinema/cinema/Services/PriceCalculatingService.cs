using cinema.Data;
using cinema.Models;
using cinema.Repositories;
using Microsoft.EntityFrameworkCore;

namespace cinema.Services;

public class PriceCalculatingService : IPriceCalculatingService
{

    private readonly IShowRepository _showRepository;
    
    private readonly IMovieService _movieService;

    private readonly IArrangementRepository _arrangementRepository;

    public PriceCalculatingService(IMovieService movieService, IShowRepository showRepository, IArrangementRepository arrangementRepository)
    {
        _movieService = movieService;
        _showRepository = showRepository;
        _arrangementRepository = arrangementRepository;
    }

    public double pricePerTicket(int showId)
    {
        var show = _showRepository.FindShowByIdIncludeMovie(showId);
        var premium = 0.0;
        if (show.ThreeD)
        {
            premium = 3.5;
        }
        if (show.Movie.Length > 120)
        {
            return 9 + premium;
        }
        return 8.5 + premium;
    }

    public double ticketCost(int quantity, int showId)
    {
        return pricePerTicket(showId) * quantity;
    }

    public double Discount(int ChildDiscount, int StudentDiscount, int SeniorDiscount)
    {
        return (ChildDiscount + StudentDiscount + SeniorDiscount) * 1.5;
    }

    public double Premium(int Popcorn)
    {
        return Popcorn * 2.5;
    }

    public double ArrangementCost(Arrangements arrangement)
    {
        var normal = _arrangementRepository.FindByName("normale");
        var party = _arrangementRepository.FindByName("kinderfeestje");
        var vp = _arrangementRepository.FindByName("vip");

        

        return arrangement switch
        {
            Arrangements.kinderfeestje => party.Price,
            Arrangements.vip => vp.Price,
            Arrangements.normale => normal.Price
        };
    }

    public double OrderCost(double Discount, double Premium, double SubTotalCost, double arrangementCost)
    {
        return SubTotalCost - Discount + Premium + arrangementCost;
    }
}