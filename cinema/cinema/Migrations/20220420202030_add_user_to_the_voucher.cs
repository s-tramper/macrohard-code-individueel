﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cinema.Migrations
{
    public partial class add_user_to_the_voucher : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "userId",
                table: "Voucher",
                type: "nvarchar(450)",
                nullable: false,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Voucher_userId",
                table: "Voucher",
                column: "userId");

            migrationBuilder.AddForeignKey(
                name: "FK_Voucher_AspNetUsers_userId",
                table: "Voucher",
                column: "userId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Voucher_AspNetUsers_userId",
                table: "Voucher");

            migrationBuilder.DropIndex(
                name: "IX_Voucher_userId",
                table: "Voucher");

            migrationBuilder.AlterColumn<int>(
                name: "userId",
                table: "Voucher",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)");
        }
    }
}
