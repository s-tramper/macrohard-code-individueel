﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace cinema.Migrations
{
    public partial class add_user_to_voucher : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_voucher_VoucherType_VoucherTypeId",
                table: "voucher");

            migrationBuilder.DropPrimaryKey(
                name: "PK_voucher",
                table: "voucher");

            migrationBuilder.RenameTable(
                name: "voucher",
                newName: "Voucher");

            migrationBuilder.RenameIndex(
                name: "IX_voucher_VoucherTypeId",
                table: "Voucher",
                newName: "IX_Voucher_VoucherTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Voucher",
                table: "Voucher",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Voucher_VoucherType_VoucherTypeId",
                table: "Voucher",
                column: "VoucherTypeId",
                principalTable: "VoucherType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Voucher_VoucherType_VoucherTypeId",
                table: "Voucher");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Voucher",
                table: "Voucher");

            migrationBuilder.RenameTable(
                name: "Voucher",
                newName: "voucher");

            migrationBuilder.RenameIndex(
                name: "IX_Voucher_VoucherTypeId",
                table: "voucher",
                newName: "IX_voucher_VoucherTypeId");

            migrationBuilder.AddPrimaryKey(
                name: "PK_voucher",
                table: "voucher",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_voucher_VoucherType_VoucherTypeId",
                table: "voucher",
                column: "VoucherTypeId",
                principalTable: "VoucherType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
