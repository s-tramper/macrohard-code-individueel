using cinema.Identity;

namespace cinema.Repositories;

public interface IUserRepositroy
{
    CinemaIdentityUser FindByUsername(string user);
    CinemaIdentityUser FindByName(string? identityName);
}