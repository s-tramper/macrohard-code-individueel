using cinema.Data;
using cinema.Identity;

namespace cinema.Repositories;

public class UserRepository : IUserRepositroy
{
    private readonly CinemaContext _context;

    public UserRepository(CinemaContext cinemaContext)
    {
        _context = cinemaContext;
    }
    public CinemaIdentityUser FindByUsername(string user)
    {
        return _context.CinemaIdentityUser.First(u => u.UserName == user);

    }

    public CinemaIdentityUser FindByName(string? identityName)
    {
       return _context.CinemaIdentityUser.First(u => u.UserName == identityName);
    }
}