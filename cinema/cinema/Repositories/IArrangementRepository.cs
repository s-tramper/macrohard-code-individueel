using cinema.Models;

namespace cinema.Repositories;

public interface IArrangementRepository
{
    public IQueryable<Arrangement> FindAllArrangements();
    public Arrangement FindById(int id);
    public Arrangement FindByName(string name);
    public void save();
}