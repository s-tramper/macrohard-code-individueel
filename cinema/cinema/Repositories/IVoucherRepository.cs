using cinema.Identity;
using cinema.Models;

namespace cinema.Repositories;

public interface IVoucherRepository
{
    public IQueryable<VoucherType> FindAllVoucherTypes();
    public VoucherType FindById(int id);
    void AddNewVoucher(Voucher voucher);
    void save();
    object findByUser(CinemaIdentityUser currentUser);
}