using cinema.Data;
using cinema.Models;

namespace cinema.Repositories;

public class ArrangementRepository : IArrangementRepository
{
    private readonly CinemaContext _context;

    public ArrangementRepository(CinemaContext cinemaContext)
    {
        _context = cinemaContext;
    }
    
    public IQueryable<Arrangement> FindAllArrangements()
    {
        return _context.Arrangement;
    }

    public Arrangement FindById(int id)
    {
        return _context.Arrangement.Find(id);
    }

    public Arrangement FindByName(string name)
    {
        return _context.Arrangement.Where(a => a.Name == name).First();
    }
    
    public void save()
    {
        _context.SaveChanges();
    }
}