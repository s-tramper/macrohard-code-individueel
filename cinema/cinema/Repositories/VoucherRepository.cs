using cinema.Data;
using cinema.Identity;
using cinema.Models;
using Microsoft.EntityFrameworkCore;

namespace cinema.Repositories;

public class VoucherRepository : IVoucherRepository
{
    private readonly CinemaContext _context;

    public VoucherRepository(CinemaContext context)
    {
        _context = context;
    }
    public IQueryable<VoucherType> FindAllVoucherTypes()
    {
        return _context.VoucherType;
    }

    public VoucherType FindById(int id)
    {
        return _context.VoucherType.Find(id); 
    }

    public void AddNewVoucher(Voucher voucher)
    {
        _context.Voucher.Add(voucher);
    }

    public void save()
    {
        _context.SaveChanges();
    }

    public object findByUser(CinemaIdentityUser currentUser)
    {
        return _context.Voucher.Where(v => v.user == currentUser).Include(v => v.VoucherType);
    }
}