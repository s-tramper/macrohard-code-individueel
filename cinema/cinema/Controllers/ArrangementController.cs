using cinema.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace cinema.Controllers;

public class ArrangementController : Controller
{
    private readonly IArrangementRepository _arrangementRepository;

    public ArrangementController(IArrangementRepository arrangementRepository)
    {
        _arrangementRepository = arrangementRepository;
    }

    [HttpGet]
    [Authorize(Policy = "Manager")]
    [Route("/arrangements")]
    public IActionResult Arrangements([FromQuery] string arrangement,[FromQuery] int price)
    {
        if (!string.IsNullOrEmpty(arrangement))
        {
            ViewBag.SuccessMessage = "De prijs van " + arrangement + " arrangement is nu " +price + " euro";
        }
        ViewBag.arrangements = _arrangementRepository.FindAllArrangements();
        return View();
    }


    [HttpPost]
    [Authorize(Policy = "Manager")]
    [Route("/arrangement-aanpassen")]
    public IActionResult Edit([FromForm] int price, [FromForm] int id)
    {
        var arrangement = _arrangementRepository.FindById(id);
        arrangement.Price = price;
        _arrangementRepository.save();

        return RedirectToAction(nameof(Arrangements), new {arrangement = arrangement.Name,price = arrangement.Price});
    }
}