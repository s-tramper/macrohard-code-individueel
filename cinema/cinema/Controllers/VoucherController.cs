using cinema.Data;
using cinema.Models;
using cinema.Repositories;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using IronBarCode;

namespace cinema.Controllers;

public class VoucherController : Controller
{
    private readonly IVoucherRepository _voucherRepository;

    private readonly IUserRepositroy _userRepository;
    
    public VoucherController(IVoucherRepository voucherRepository, IUserRepositroy userRepository)
    {
        _voucherRepository = voucherRepository;
        _userRepository = userRepository;
    }

    [HttpGet]
    [Authorize(Roles="Beheerder,Manager,Kassamedewerker,Back office Manager")]
    [Route("/bioscoop-bonnen")]
    public IActionResult Index([FromQuery] string arrangement,[FromQuery] int price)
    {
        ViewBag.VoucherTypes = _voucherRepository.FindAllVoucherTypes();
        return View();
    }
    
    [HttpGet]
    [Authorize(Roles="Beheerder,Manager,Kassamedewerker,Back office Manager")]
    [Route("/Voucher/Create")]
    public IActionResult Create([FromQuery] int id)
    {
        ViewBag.VoucherType = _voucherRepository.FindById(id);
        return View();
    }

    [HttpGet]
    [Authorize(Roles="Beheerder,Manager,Kassamedewerker,Back office Manager")]
    [Route("/Voucher/Overview")]
    public IActionResult Overview([FromQuery] int voucherId, [FromQuery] int quantity)
    {
        VoucherType voucherType = _voucherRepository.FindById(voucherId);
        ViewBag.Quantity = quantity;
        ViewBag.TotalPrice = voucherType.price * quantity;
        ViewBag.VoucherType = voucherType;
        return View();
    }
    
    [HttpPost]
    [Authorize(Roles="Beheerder,Manager,Kassamedewerker,Back office Manager")]
    public RedirectToActionResult Buy([FromForm] int voucherTypeId, [FromForm] int quantity,[FromForm] string user)
    {
        VoucherType voucherType = _voucherRepository.FindById(voucherTypeId);
        ViewBag.Quantity = quantity;
        ViewBag.TotalPrice = voucherType.price * quantity;
        ViewBag.VoucherType = _voucherRepository.FindById(voucherTypeId);
        Random rnd = new Random();

        var currentUser = _userRepository.FindByUsername(user);
        var code = rnd.Next(1, 100000);

        if (voucherType.name == "1 bioscoopbon")
        {
            GeneratedBarcode barcode = IronBarCode.BarcodeWriter.CreateBarcode(code.ToString() + voucherType.Id.ToString() , BarcodeEncoding.QRCode);
            ViewBag.barcode = barcode.SaveAsImage(Directory.GetCurrentDirectory() + "\\wwwroot\\images\\" + voucherType.Id + code +".png");
        }else if(voucherType.name == "MaDiWoDo")
        {
            for (int i = 0; i < 4; i++)
            {
                GeneratedBarcode barcode = IronBarCode.BarcodeWriter.CreateBarcode(code.ToString() + voucherType.Id.ToString() + i.ToString() , BarcodeEncoding.QRCode);
                ViewBag.barcode = barcode.SaveAsImage(Directory.GetCurrentDirectory() + "\\wwwroot\\images\\" + voucherType.Id + code + i + ".png");
            }
        }else if (voucherType.name == "10 rittenkaart")
        {
            for (int i = 0; i < 10; i++)
            {
                GeneratedBarcode barcode = IronBarCode.BarcodeWriter.CreateBarcode(code.ToString() + voucherType.Id.ToString() + i.ToString() , BarcodeEncoding.QRCode);
                ViewBag.barcode = barcode.SaveAsImage(Directory.GetCurrentDirectory() + "\\wwwroot\\images\\" + voucherType.Id + code + i + ".png");
            }
        }
        
        for (int i = 0; i < quantity; i++)
        {
            _voucherRepository.AddNewVoucher(
                new Voucher()
                {
                    VoucherType = voucherType,
                    user = currentUser,
                    code = code
                }
                );
        }

        _voucherRepository.save();
       
        return RedirectToAction(nameof(Result), new {message = "Bedankt, het kopen van het kaartje is gelukt"});

    }
    
    
    [HttpGet]
    [Authorize(Roles="Beheerder,Manager,Kassamedewerker,Back office Manager")]
    [Route("/vouchers")]
    public IActionResult Result()
    {
        var currentUser = _userRepository.FindByName(User.Identity.Name);

        ViewBag.Vouchers = _voucherRepository.findByUser(currentUser);
        return View();
    }

}