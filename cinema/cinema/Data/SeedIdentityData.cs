using System.Security.Claims;
using cinema.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace cinema.Data;

public static class SeedIdentityData
{
    public static void Initialize(IServiceProvider serviceProvider, IConfiguration configuration)
        {
            serviceProvider = serviceProvider.CreateScope().ServiceProvider;

            RoleManager<CinemaIdentityRole> roleManager =
              serviceProvider.GetRequiredService<RoleManager<CinemaIdentityRole>>();

            if (!roleManager.RoleExistsAsync("Beheerder").Result)
            {
                CinemaIdentityRole role = new CinemaIdentityRole();
                role.Name = "Beheerder";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync("Back office Manager").Result)
            {
                CinemaIdentityRole role = new CinemaIdentityRole();
                role.Name = "Back office Manager";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }

            if (!roleManager.RoleExistsAsync("Kassamedewerker").Result)
            {
                CinemaIdentityRole role = new CinemaIdentityRole();
                role.Name = "Kassamedewerker";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
            
            if (!roleManager.RoleExistsAsync("Manager").Result)
            {
                CinemaIdentityRole role = new CinemaIdentityRole();
                role.Name = "Manager";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }
            
            if (!roleManager.RoleExistsAsync("Abonnementhouder").Result)
            {
                CinemaIdentityRole role = new CinemaIdentityRole();
                role.Name = "Abonnementhouder";
                IdentityResult roleResult = roleManager.CreateAsync(role).Result;
            }

            UserManager<CinemaIdentityUser> userManager =
                serviceProvider.GetRequiredService<UserManager<CinemaIdentityUser>>();

            if (userManager.FindByEmailAsync("gebruiker1@outlook.com").Result == null)
            {
                CinemaIdentityUser user = new CinemaIdentityUser();
                user.UserName = "gebruiker1";
                user.Email = "gebruiker1@outlook.com";

                IdentityResult result = userManager.CreateAsync(user, "Welkom12345!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Beheerder").Wait();
                }
            }

            if (userManager.FindByEmailAsync("gebruiker2@outlook.com").Result == null)
            {
                CinemaIdentityUser user = new CinemaIdentityUser();
                user.UserName = "gebruiker2";
                user.Email = "gebruiker2@outlook.com";

                IdentityResult result = userManager.CreateAsync(user, "Welkom12345!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Back office Manager").Wait();
                }
            }

            if (userManager.FindByEmailAsync("gebruiker3@outlook.com").Result == null)
            {
                CinemaIdentityUser user = new CinemaIdentityUser();
                user.UserName = "gebruiker3";
                user.Email = "gebruiker3@outlook.com";

                IdentityResult result = userManager.CreateAsync(user, "Welkom12345!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Kassamedewerker").Wait();
                }
            }

            if (userManager.FindByEmailAsync("gebruiker4@outlook.com").Result == null)
            {
                CinemaIdentityUser user = new CinemaIdentityUser();
                user.UserName = "gebruiker4";
                user.Email = "gebruiker4@outlook.com";

                    IdentityResult result = userManager.CreateAsync(user, "Welkom12345!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Manager").Wait();
                }
            }
            
            if (userManager.FindByEmailAsync("gebruiker5@outlook.com").Result == null)
            {
                CinemaIdentityUser user = new CinemaIdentityUser();
                user.UserName = "gebruiker5";
                user.Email = "gebruiker5@outlook.com";

                IdentityResult result = userManager.CreateAsync(user, "Welkom12345!").Result;

                if (result.Succeeded)
                {
                    userManager.AddToRoleAsync(user, "Abonnementhouder").Wait();
                }
            }
        }

}
